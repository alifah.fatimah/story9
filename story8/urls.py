from django.urls import path
from .views import bookSearchView, fungsi_data

app_name = 'story8'

urlpatterns = [
    path('', bookSearchView, name='book'),
    path('data/', fungsi_data, name='fungsi_data')
]
