from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
import json
from django.core import serializers
import requests

# Create your views here.
def bookSearchView(request):
    return render(request, 'book.html')

def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    return JsonResponse(ret.json())
